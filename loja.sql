-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 02/09/2013 às 08h42min
-- Versão do Servidor: 5.5.32
-- Versão do PHP: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text NOT NULL,
  `email` text NOT NULL,
  `datacadastro` datetime NOT NULL,
  `senha` varchar(80) NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `admin`
--

INSERT INTO `admin` (`id`, `nome`, `email`, `datacadastro`, `senha`, `status`) VALUES
(1, 'Lucian Marques', 'lucian.rochati@gmail.com', '2013-08-30 00:00:00', '40271d92bddce8e1d2cb88acab0af900', 'Ativo'),
(2, 'Before', 'eduardo@before.com.br', '2013-09-02 14:39:27', 'cba23b9adb9fc7c1a45457a53e10e3f5', 'Ativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text NOT NULL,
  `descricao` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  `datacadastro` datetime NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`, `descricao`, `parent_id`, `datacadastro`, `status`) VALUES
(1, 'Telefonia', '', 0, '2013-09-01 19:03:46', 'Ativo'),
(2, 'Celulares', '', 1, '2013-09-01 19:03:59', 'Ativo'),
(5, 'MemÃ³ria RAM', 'memÃ³ria DDRs', 7, '2013-09-01 19:07:34', 'Ativo'),
(6, 'Smartphones', '', 2, '2013-09-01 20:04:20', 'Ativo'),
(7, 'InformÃ¡tica', '', 0, '2013-09-01 20:29:19', 'Ativo'),
(8, 'Placas de vÃ­deo', '', 7, '2013-09-01 20:30:19', 'Ativo'),
(9, 'Gforce', '', 8, '2013-09-01 20:30:28', 'Ativo'),
(10, 'DDR3', '', 5, '2013-09-01 22:11:05', 'Ativo'),
(11, 'Notebooks', '', 7, '2013-09-01 22:14:17', 'Ativo'),
(12, 'ACER', '', 11, '2013-09-01 22:14:25', 'Ativo'),
(14, 'HP', '', 11, '2013-09-01 23:19:43', 'Ativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text NOT NULL,
  `resumo` text NOT NULL,
  `descricao` text NOT NULL,
  `img` varchar(100) NOT NULL,
  `categoria` varchar(15) DEFAULT NULL,
  `datacadastro` datetime NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `resumo`, `descricao`, `img`, `categoria`, `datacadastro`, `status`) VALUES
(7, 'Samsumg Galaxy S||', '', '', 'e813c7a854e4265fcb5a524fdd80229a.jpg', '1,2,6', '2013-09-01 20:05:38', 'Ativo'),
(8, 'Galaxy Ace DUO', '', '', 'bf329d8cd3f84b62f32b68c6bb0aa799.jpg', '1,2,6', '2013-09-01 20:26:42', 'Ativo'),
(9, 'MemÃ³ria RAM DDR3', '', '', 'aaf59522642e1f0ba41902af0abfdf2f.jpg', '7,5,10', '2013-09-01 20:29:59', 'Ativo'),
(12, 'Notebook HP Pavilion', '', '', '4dd0e2184791028855b3c2e1f082968b.jpg', '7,11,14', '2013-09-01 23:21:32', 'Ativo');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
