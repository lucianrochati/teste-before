<?php
	require_once(dirname(dirname(__FILE__))."/includes/Control/Validacoes.php");
	
	if($_SESSION['email']){
		$validacao = new Validacoes();
		unset($_SESSION);
		$validacao->redirectUserAdmin("../index.php");
	}
	