<?php
	require_once(dirname(dirname(__FILE__))."/includes/Configuracoes.php");
	require_once(dirname(dirname(__FILE__))."/includes/Produtos.php");
	require("header.php");
	
?>
<div class="container">
	<div class="containerMenu">
		<?php require("menu.php");?>
	</div>
	<div class="meio">
		<span>Página: Produtos</span>
		<h2>Listagem de produtos</h2>
		<?php 
		$produtos = new Produtos();
		$lista = $produtos->getProdutos();
		
		
		if($lista != ""){
		?>
		<div style="width:1000px; float:left; height:40px;">
		<form name="categorias" action="acoes.php" method="post">
		<input type="hidden" name="acao" value="excluir">
		<input type="hidden" name="pagina" value="produtos">
		<div style="float:left; margin-right:20px; height:40px;">
			<a href="novoProduto.php"><img src="images/btnAdicionar.png" title="Adicionar novo Produto"></a>
		</div>
		<table id="tabela" style="width:970px;font-family: Arial;font-size: 12px;color: #999;">
			
            <thead>
                <tr style="text-align:left; background: #F3F3F3; font-family:Arial;font-size:14px; height: 25px;">
                    <th></th>
                    <th>Código</th>
                    <th>Foto</th>
                    <th>Nome</th>
                    <th>Categoria</th>
                    <th>Data de cadastro</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>         
            </thead>
			<?php foreach($lista as $cadaProd){
				$categorias = $produtos->getCategoriasProduto($cadaProd->id);
			
			?>
			<tbody>
                <tr>
                	<td><input type="checkbox" name="ids[]" value="<?php echo $cadaProd->id?>"></td>
                	<td><?php echo $cadaProd->id?></td>
                    <td><?php if($cadaProd->img){?><img src="uploads/<?php echo $cadaProd->img?>" width="60" height="40"><?php }?>
                    <td><?php echo $cadaProd->nome?></td>
                    <td><?php
                    	if($categorias){
                    		foreach($categorias as $cat){
	                    		if($cat['nome']){
	                    				echo " > ".$cat['nome'];
	                    		}
                    		}
						}?></td>
                    <td><?php echo $cadaProd->datacadastro?></td>
                    <td><?php echo $cadaProd->status;?></td>
                    <td><input type="image" src="http://localhost/teste/admin/images/remover.png" width="30" height="28">
                    	<a href="editarProduto.php?prod=<?php echo $cadaProd->id;?>"><img src="http://localhost/teste/admin/images/btnAlterar.gif"></a></td>
                </tr>
              </tbody>  
              </form>	         
    		<?php }?>
		 
        </table>
        
		</div>
		<?php }else{?>
			Nenhuma Categoria encontrada!
		<?php }?>
	</div>
	
</div>