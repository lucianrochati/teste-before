<?php
	ini_set("display_errors",1);
	require_once("../includes/Configuracoes.php");
	require_once("../includes/Produtos.php");
	require_once("../includes/Categorias.php");
	require("header.php");
	$produtos = new Produtos();
	
	$prod = $_GET['prod'];
	
	$produto = $produtos->getProdutoPorId($prod);
	
?>

<script>

	$(document).ready(function() {
		$(".todasCategorias").hide();
		
			$("#alterar").click(function(){
				$('input:checkbox').attr('checked',false);
			$(".categoriasInseridas").hide("slow");
			$(".todasCategorias").show("slow");
		});

		$(".catPai").click(function(){
			$(".catPai"+this.value).show("");
			
		});
		$(".subCat").click(function(){
			$(".catPai"+this.value).show("");
		});
	});

</script>
<div class="container">
	<div class="containerMenu">
		<?php require("menu.php");?>
	</div>
	<div class="meio">
		<span>Página: Produtos > <?php echo $produto[0]->nome?></span>
		<h2>Editando produto #<?php echo $produto[0]->id?></h2>
		<div style="float:right"><a href="javascript:history.go(-1)"><img src="images/btn-voltar.jpg"></a></div>
		<div style="width:1000px; float:left; height:40px;">
		<form name="produtos" action="acoes.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="acao" value="editar">
		<input type="hidden" name="pagina" value="produtos">
		<input type="hidden" name="id" value="<?php echo $produto[0]->id;?>">
		<div style="width:350px; float:left; border:1px solid #CCC; margin-left:20px; padding:10px;">
			<div style="float:left; width:200px;">
				<label>Nome</label>
				<input type="text" style="width: 300px;border: 1px solid #CCC;padding: 6px;"name="nome" value="<?php echo $produto[0]->nome?>">
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Resumo</label>
				<textarea name="resumo" style="width: 300px;border: 1px solid #CCC;height:50px;"><?php echo $produto[0]->resumo?></textarea>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Descrição</label>
				<textarea name="descricao" style="width: 300px;border: 1px solid #CCC;height:50px;"><?php echo $produto[0]->descricao?></textarea>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Foto</label>
				<?php if($produto[0]->img){?>
					<input type="file" name="img">
					<input type="hidden" name="imgAntiga" value="<?php echo $produto[0]->img?>">
					<img src="uploads/<?php echo $produto[0]->img?>" width="120" height="90">
				<?php }else{?>
					<input type="file" name="img">
				<?php }?>
			</div>
			<div style="float:left; width:325px; margin-top:10px">
				<label>Categorias</label>
				<br/>
				<?php
					$cat = new Categorias();
					
					?>
					<div class="todasCategorias">
						<?php 
						$query = mysql_query('SELECT * FROM categorias ORDER BY parent_id');
			
						while( $row = mysql_fetch_object($query) )
						{
							$lista[$row->parent_id][$row->id] = array( 'nome' => $row->nome );
						}
						$cat->listarCategoriasProduto($lista);
						?>
					</div>
					<div class="categoriasInseridas" style="border:1px solid #ccc; display:block; width:330px; display:block;">
						<?php 
						$categorias = $produtos->getCategoriasProduto($prod);
						
						foreach($categorias as $cada){
							$listaInseridas[$cada['pai']][$cada['cat_id']] = array( 'nome' => $cada['nome']);
						}
						$cat->editarCategoriasProduto($listaInseridas);
						?>
						
					</div>
					<div style="float: right;width: 100px;text-align: right;height: 10px;margin-top: 10px; " id="alterar">
						<img title="Alterar categoria" id="alterar" src="images/btnAlterar.gif" width="30" height="28" style="cursor:pointer;">
					</div>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Status</label>
				<select name="status">
					<option value="Ativo">Ativo</option>
					<option value="Inativo">Inativo</option>
				</select>
			</div>
			<div style="float:right;  margin-top:10px;float: right;clear: both;">
				<input type="image" title="Salvar" src="images/btn_salvar.jpg" value="Salvar">
			</div>
		</div>
		</form>
		 </div>
	</div>
	
</div>