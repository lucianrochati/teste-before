<?php

	require_once("../includes/Configuracoes.php");
	require_once("../includes/Categorias.php");
	require("header.php");
	
	$categorias = new Categorias();
	$categorias = $categorias->getCategorias();
	
?>
<div class="container">
	<div class="containerMenu">
		<?php require("menu.php");?>
	</div>
	<div class="meio">
		<span>Página: Categorias > Telefonia</span>
		<h2>Editando categoria #1</h2>
		
		<div style="width:1000px; float:left; height:40px;">
		<form name="categorias" action="acoes.php" method="post">
		<input type="hidden" name="acao" value="inserir">
		<input type="hidden" name="pagina" value="categorias">
		<div style="width:350px; float:left; border:1px solid #CCC; margin-left:20px; padding:10px;">
			<div style="float:left; width:200px;">
				<label>Nome</label>
				<input type="text" name="nome" style="width: 300px;border: 1px solid #CCC;padding: 6px;" >
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Descrição</label>
				<textarea name="descricao" style="width: 300px;border: 1px solid #CCC;padding: 6px; height:50px;" ></textarea>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Pai</label>
				<select name="pai">
					<option value="0">Nenhuma</option>
					<?php foreach($categorias as $cadaCat){?>
						<option value="<?php echo $cadaCat->id?>"><?php echo $cadaCat->nome;?></option>
					<?php }?>
				</select>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Status</label>
				<select name="status">
					<option value="Ativo">Ativo</option>
					<option value="Inativo">Ativo</option>
				</select>
			</div>
			<div style="float:right; width:200px; margin-top:10px">
				<input type="submit" value="Salvar">
			</div>
		</div>
		</form>
		 </div>
	</div>
	
</div>