<?php
ini_set("display_errors",1);

require_once(dirname(dirname(__FILE__))."/includes/Configuracoes.php");
require_once(dirname(dirname(__FILE__))."/includes/Categorias.php");
require_once(dirname(dirname(__FILE__))."/includes/Produtos.php");
require_once(dirname(dirname(__FILE__))."/includes/Control/Validacoes.php");

$validacao = new Validacoes();
$categorias = new Categorias();
$produtos   = new Produtos();

$acao = $_POST['acao'];
if($acao){
	switch ($acao){
		
		case "excluir":
			
			if(empty($_POST['ids']) && $_POST['pagina'] == "categorias"){
				$validacao->retornoJS("Seleciona um categoria para excluir","categorias.php");
			}elseif(empty($_POST['ids']) && $_POST['pagina'] == "produtos"){
				$ids = $_POST['ids'];
				$validacao->retornoJS("Seleciona um produto para excluir","produtos.php");
			}
			if($_POST['pagina'] == "categorias"){
				$retorno = $categorias->excluirCategoria($ids);	
				$validacao->retornoJS($retorno,"categorias.php");
			}elseif($_POST['pagina'] == "produtos"){
				$retorno = $produtos->excluirProduto($ids);	
				$validacao->retornoJS($retorno,"produtos.php");
			}
			
		case "editar":
			if($_POST['pagina'] == "categorias"){
				$array = $_POST;
				$retorno = $categorias->alterarCategoria($array);
				$validacao->retornoJS($retorno,"categorias.php");
			}elseif($_POST['pagina'] == "produtos"){
				$array = $_POST;
				$retorno = $produtos->alterarProduto($array);
				$validacao->retornoJS($retorno,"produtos.php");	
			}
		case "inserir":
			if($_POST['pagina'] == "categorias"){
				$array = $_POST;
				$retorno = $categorias->cadastrarCategoria($array);
				$validacao->retornoJS($retorno, "categorias.php");
			}elseif($_POST['pagina'] == "produtos"){
				$array = $_POST;
				$retorno = $produtos->cadastrarProduto($array);
				$validacao->retornoJS($retorno,"produtos.php");		
			}
		
	}		
}else{
	
}