<?php

	require_once("../includes/Configuracoes.php");
	require_once("../includes/Categorias.php");
	require("header.php");
?>
<div class="container">
	<div class="containerMenu">
		<?php require("menu.php");?>
	</div>
	<div class="meio">
		<span>Página: Categorias</span>
		<h2>Listagem de categorias</h2>
		<?php 
		$categorias = new Categorias();
		$lista = $categorias->getCategorias();

		if($lista){
		?>
		<div style="width:1000px; float:left; height:40px;">
		<form name="categorias" action="acoes.php" method="post">
		<input type="hidden" name="acao" value="excluir">
		<input type="hidden" name="pagina" value="categorias">
		<div style="float:left; margin-right:20px; height:40px;">
			<a href="novaCategoria.php"><img src="images/btnAdicionar.png" title="Adicionar nova categoria"></a>
		</div>
		 <table id="tabela" style="width:970px;font-family: Arial;font-size: 12px;color: #999;">
			
            <thead>
                <tr style="text-align:left; background: #F3F3F3; font-family:Arial;font-size:14px; height: 25px;">
                    <th></th>
                    <th>Código</th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Data de cadastro</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>         
            </thead>
			<?php foreach($lista as $cadaCat){?>
			<tbody>
                <tr>
                	<td><input type="checkbox" name="ids[]" value="<?php echo $cadaCat->id?>"></td>
                    <td><?php echo $cadaCat->id?></td>
                    <td><?php echo $cadaCat->nome?></td>
                    <td><?php echo $cadaCat->descricao;?></td>
                    <td><?php echo date("d/m/y H:i",strtotime($cadaCat->datacadastro))?></td>
                    <td><?php echo $cadaCat->status;?></td>
                    <td><input type="image" src="http://localhost/teste/admin/images/remover.png" width="30" height="28" title="Remover">
                    	<a href="editarCategoria.php?cat=<?php echo $cadaCat->id;?>"><img src="http://localhost/teste/admin/images/btnAlterar.gif" title="Alterar"></a></td>
                </tr>
              </tbody>  
              </form>	         
    		<?php }?>
		 
        </table>
        
		</div>
		<?php }else{?>
			Nenhuma Categoria encontrada!
		<?php }?>
	</div>
	
</div>