<?php

	require_once("../includes/Configuracoes.php");
	require_once("../includes/Categorias.php");
	require("header.php");
	$categorias = new Categorias();
	$cat = $_GET['cat'];
	
	$categoria = $categorias->getCategoriasPorId($cat);
	$categorias = $categorias->getCategorias();
	
?>
<div class="container">
	<div class="containerMenu">
		<?php require("menu.php");?>
	</div>
	<div class="meio">
		<span>Página: Categorias > <?php echo $categoria[0]->nome?></span>
		<h2>Editando categoria #<?php echo $categoria[0]->id?></h2>
		<div style="float:right"><a href="javascript:history.go(-1)"><img src="images/btn-voltar.jpg"></a></div>
		<div style="width:1000px; float:left; height:40px;">
		<form name="categorias" action="acoes.php" method="post">
		<input type="hidden" name="acao" value="editar">
		<input type="hidden" name="pagina" value="categorias">
		<input type="hidden" name="id" value="<?php echo $categoria[0]->id;?>">
		<div style="width:350px; float:left; border:1px solid #CCC; margin-left:20px; padding:10px;">
			<div style="float:left; width:200px;">
				<label>Nome</label>
				<input type="text" style="width: 300px;border: 1px solid #CCC;padding: 6px;" name="nome" value="<?php echo $categoria[0]->nome?>">
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Descrição</label>
				<textarea name="descricao" style="width: 300px;border: 1px solid #CCC;height:50px;"><?php echo $categoria[0]->descricao?></textarea>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Pai</label>
				<select name="pai">
					<option value="0">Nenhuma</option>
					<?php foreach($categorias as $cadaCat){
							if($categoria[0]->parent_id && $cadaCat->id == $categoria[0]->parent_id){
								$id = $categoria[0]->parent_id; 
							}else{
								$id = $cadaCat->id;
							
							}?>
					<?php if($categoria[0]->id != $cadaCat->id){?>
					<option value="<?php echo $id?>"<?php if($id == $categoria[0]->parent_id) echo "selected";?>>
						<?php echo $cadaCat->nome;?>
					</option>
					<?php } 
					}?>
				</select>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Status</label>
				<select name="status">
					<option value="Ativo">Ativo</option>
					<option value="Inativo">Ativo</option>
				</select>
			</div>
			<div style="float:right;  margin-top:10px;float: right;clear: both;">
				<input type="image" title="Salvar" src="images/btn_salvar.jpg" value="Salvar">
			</div>
		</div>
		</form>
		 </div>
	</div>
	
</div>