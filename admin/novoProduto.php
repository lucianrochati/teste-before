<?php

	require_once("../includes/Configuracoes.php");
	require_once("../includes/Categorias.php");
	require("header.php");
	
	$categorias = new Categorias();
	
?>
<script>

	$(document).ready(function() {

		$(".catPai").click(function(){
			$(".catPai"+this.value).show("");
			
		});
		$(".subCat").click(function(){
			$(".catPai"+this.value).show("");
		});
	});

</script>
<div class="container">
	<div class="containerMenu">
		<?php require("menu.php");?>
	</div>
	<div class="meio">
		<span>Página: Produtos > novo</span>
		<h2>Cadastrando novo produto</h2>
		
		<div style="width:1000px; float:left; height:40px;">
		<form name="produtos" action="acoes.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="acao" value="inserir">
		<input type="hidden" name="pagina" value="produtos">

		<div style="width:350px; float:left; border:1px solid #CCC; margin-left:20px; padding:10px;">
			<div style="float:left; width:200px;">
				<label>Nome</label>
				<input type="text" name="nome" style="width: 332px;padding: 4px;border: 1px solid #CCC;">
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Resumo</label>
				<textarea name="resumo" style="width: 332px;padding: 4px;border: 1px solid #CCC;"></textarea>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Descrição</label>
				<textarea name="descricao" style="width: 332px;padding: 4px;border: 1px solid #CCC;"></textarea>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Foto</label>
				<input type="file" name="img">
			</div>
			<div style="float: left;width: 333px; font-family:Arial; color:#999; font-size:14px;margin-top: 10px;padding: 7px;">
				<label>Categorias</label>
				<fieldset style="margin-top:10px">
				<?php
					$query = mysql_query('SELECT * FROM categorias ORDER BY parent_id');
		
					while( $row = mysql_fetch_object($query) )
					{
						$lista[$row->parent_id][$row->id] = array( 'nome' => $row->nome );
					}
					$categorias->listarCategoriasProduto($lista);
					?>
				
				</fieldset>
			</div>
			<div style="float:left; width:200px; margin-top:10px">
				<label>Status</label>
				<select name="status">
					<option value="Ativo">Ativo</option>
					<option value="Inativo">Ativo</option>
				</select>
			</div>
			<div style="float:right; width:200px; margin-top:10px">
				<input type="submit" value="Salvar">
			</div>
		</div>
		</form>
		 </div>
	</div>
	
</div>