<?php

require_once (dirname(dirname(__FILE__))."/includes/Model/dao/Produtos.php");
require_once(dirname(dirname(__FILE__))."/includes/Control/Validacoes.php");


class Produtos{
	
	private $produtos;
	private $nome;
	private $validacao;
	
	function  __construct(){
		$this->produtos = new ProdutosDAO();
		$this->validacao = new Validacoes();
	}
	
	public function getProdutos(){
		return $this->produtos->listarProdutos();
	}
	
	public function getProdutoPorId($id){
		return $this->produtos->getProdutoPeloId($id);
	}
	
	public function getCategoriasProduto($prod){
		return $this->produtos->selecionarCategoriasProduto($prod);
	}
	
	public function excluirProduto($id){
		return $this->produtos->excluirProduto($id);
	}
	
	public function cadastrarProduto($array){
		if(isset($array['categorias']))
			$categorias = implode(",",$array['categorias']);
			else{
				$this->validacao->retornoJS("Obrigatoriamente deve ser selecionada uma categoria","novoProduto.php");
			}
		
			if($_FILES['img'] > 0){
				$nome_foto = $this->fazerUploadFoto($_FILES);
			}
		return $this->produtos->inserirProdutos($categorias,$array,$nome_foto);	
	}
	
	public function alterarProduto($array){
		
		if(isset($array['categorias']))
			$categorias = implode(",",$array['categorias']);
		else{
				$this->validacao->retornoJS("Obrigatoriamente deve ser selecionada uma categoria","editarProduto.php?prod={$array['id']}");
			}
		if($_FILES['img'] && $_FILES['img']['size'] > 0){
			$nome_foto = $this->fazerUploadFoto($_FILES);
			}else if($array['imgAntiga']){
				$nome_foto = $array['imgAntiga'];
			}
		
		return $this->produtos->editarProduto($categorias,$array,$nome_foto);
	}
	
	public function setNome($nome){
		$this->nome = $nome;
	}
	
	public function getNome(){
		return $this->nome;
	}
	
	public function fazerUploadFoto($file){
		
		$arquivo = $file['img']['name'];
	 	$extensao = strtolower(end(explode('.', $arquivo)));
	 	$nome_imagem = md5(uniqid(time())) . "." . $extensao;
	 	$caminho_imagem = "uploads/" . $nome_imagem;
	 	$retorno = move_uploaded_file($file['img']['tmp_name'], $caminho_imagem); 

		if($retorno){
			return $nome_imagem;
		}else{
			return false;
		}
	}
}