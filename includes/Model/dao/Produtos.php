<?php

require_once(dirname(dirname(dirname(__FILE__)))."/Configuracoes.php");
require_once(dirname(dirname(__FILE__))."/database/Conexao.php");

class ProdutosDAO extends Conexao{
	
	private $tabela = "produtos";
	private $produtos;
	
	function __construct(){
		$this->conectar();
	}
	
	public function listarProdutos(){
		
		$objeto = "";
		
		try{
			
			$sql = "SELECT * FROM {$this->tabela}";
			$rs = mysql_query($sql);

				while($resultado = mysql_fetch_object($rs)){
					$objeto[] = $resultado;
				}	
				return $objeto;	
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
		}
			
	}
	public function getProdutoPeloId($id){
		
		$objeto = "";
		
		try{
			
			$sql = "SELECT * FROM {$this->tabela} WHERE id = '{$id}'";
			$rs = mysql_query($sql);

				while($resultado = mysql_fetch_object($rs)){
					$objeto[] = $resultado;
				}	
				return $objeto;	
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
		}
		
	}
	public function editarProduto($stringCat,$array,$nome_foto){
	
		try{
			$sql = "UPDATE produtos SET
					nome = '{$array['nome']}',
					resumo = '{$array['resumo']}',
					descricao = '{$array['descricao']}',
					img		  = '{$nome_foto}',
					categoria = '{$stringCat}',
					status = '{$array['status']}'
						WHERE id = {$array['id']}";
		
				$rs = mysql_query($sql);
	
			if($rs)
				$retorno = "Produto atualizado com sucesso";
			else
				$retorno = "Erro ao tentar atualizar produto!!!";
			
			return $retorno;
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
			}
		
		
	}
	public function excluirProduto($id){
		try{
			if(is_array($id)){
		
				foreach($id as $cadaId){
					$sql = "DELETE FROM produtos WHERE id = {$cadaId}";
					$rs  = mysql_query($sql);
				}
				if($rs)
					$retorno = "Produtos excluídos com sucesso!!";
					else
						$retorno = "Houve um erro ao tentar excluir produtos, tente novamente mais tarde!";
			}else{
				$sql = "DELETE FROM produtos WHERE id = {$id}";
				$rs  = mysql_query($sql);
				if($rs)
					$retorno = "Produto excluido com sucesso!!";
				else
					$retorno = "Houve um erro ao tentar excluir produto, tente novamente mais tarde!";
			}
		}catch (Exception $e){
			echo "Exceção pega: ",  $e->getMessage(), "\n";die;
		}
	return $retorno;
	}
	
	public function inserirProdutos($strCategorias,$array,$nome_foto){

		try{
			$sql = "INSERT INTO produtos 
					VALUES(
						NULL,
						'{$array['nome']}',
						'{$array['resumo']}',
						'{$array['descricao']}',
						'{$nome_foto}',
						'$strCategorias',
						NOW(),
						'{$array['status']}'
			
					)
				";
			$rs = mysql_query($sql);

			if($rs){
				$retorno = "Produto cadastrado com sucesso";
				}else{
					$retorno = "Erro ao tentar cadastrar produto!!!";
				}
			return $retorno;
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
			}
	}
	
	public function selecionarCategoriasProduto($prod){
		
		$objeto = array();
		
		try{
			
			$sql = "SELECT categoria FROM produtos WHERE id = {$prod}";
			$rs = mysql_query($sql);
			
			while($resultado = mysql_fetch_object($rs)){
				$categorias = explode(",",$resultado->categoria);
			}
		
			foreach($categorias as $cadaCat){
				
				$sql2 = "SELECT nome, parent_id, id FROM categorias WHERE id = '{$cadaCat}'
						group by parent_id order by parent_id > 1";
				$rs2 = mysql_query($sql2);
				
				while($resultado2 = mysql_fetch_object($rs2)){
					$catMontada[$resultado2->id]['nome'] = $resultado2->nome;
					$catMontada[$resultado2->id]['pai'] = $resultado2->parent_id;
					$catMontada[$resultado2->id]['cat_id'] = $resultado2->id;	
				}
					
			}
			return $catMontada;
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
			}
			
	}
	
}