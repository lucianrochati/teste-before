<?php

require_once(dirname(dirname(dirname(__FILE__)))."/Configuracoes.php");
require_once(dirname(dirname(__FILE__))."/database/Conexao.php");

class CategoriasDAO extends Conexao{
	
	private $tabela = "categorias";
	private $categorias;
	
	function __construct(){
		$this->conectar();
	}
	
	public function listarCategorias(){
		$objeto = "";
		try{
			
			$sql = "SELECT * FROM {$this->tabela}";
			$rs = mysql_query($sql);

				while($resultado = mysql_fetch_object($rs)){
					$objeto[] = $resultado;
				}
		
			}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
			}
			if($objeto)
				return $objeto;
			return  false;			
	}
	
	public function excluirCategoria($id){

		if(is_array($id) && count($id) > 1){
	
			foreach($id as $cadaId){
				$sql = "DELETE FROM categorias WHERE id = {$cadaId}";
				$rs  = mysql_query($sql);
			}
			if($rs)
				$retorno = "Categorias excluidas com sucesso!!";
				else
					$retorno = "Houve um erro ao tentar excluir categoria, tente novamente mais tarde!";
		}else{
			$sql = "DELETE FROM categorias WHERE id = {$id[0]}";
			$rs  = mysql_query($sql);
			if($rs)
				$retorno = "Categoria excluida com sucesso!!";
			else
				$retorno = "Houve um erro ao tentar excluir categoria, tente novamente mais tarde!";
		}
	return $retorno;
	}
	
	public function mostrarCategoriaPeloId($id){
		
		$objeto = array();
		
		try{
			$sql = "SELECT * FROM categorias WHERE id = {$id}";
			$rs = mysql_query($sql);
			
			while($resultado = mysql_fetch_object($rs)){
				$objeto[] = $resultado;
			}
			/*foreach($objeto as $cadaPai){
				$objeto['filhas'] = $this->montarSubCatetorias($cadaPai->parent_id);
			}*/
			
			return $objeto;
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
			}
	}
	
	public function alterarCategoria($array){
		
		try{
			$sql = "UPDATE categorias SET
					nome = '{$array['nome']}',
					descricao = '{$array['descricao']}',
					parent_id = '{$array['pai']}',
					status = '{$array['status']}'
						WHERE id = {$array['id']}";

			$rs = mysql_query($sql);
			if($rs)
				$retorno = "Categoria atualizada com suceso";
			else
				$retorno = "Erro ao tentar atualizar categoria!!!";
			
			return $retorno;
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
			}
		
	}
	
	public function inserirCategoria($array){
		
		try{
			$sql = "INSERT into categorias 
					VALUES(
						NULL,
						'{$array['nome']}',
						'{$array['descricao']}',
						'{$array['pai']}',
						NOW(),
						'{$array['status']}'
			
					)
				";
			$rs = mysql_query($sql);
			
			if($rs)
				$retorno = "Categoria cadastrada com suceso";
			else
				$retorno = "Erro ao tentar cadastrar categoria!!!";
			
			return $retorno;
		}catch (Exception $e) {
  		
				echo "Exceção pega: ",  $e->getMessage(), "\n";die;
			
			}
	}
	
	public 	function montarCategoriasProdutos( array $categoriasTotal , $idPai = 0, $nivel = 0 ){
		
		
		foreach( $categoriasTotal[$idPai] as $catId => $cadaCat)
		{	
			
			if($nivel > 0){
				$class = "subCat";
				$display = "none";
				echo str_repeat( "\t" , $nivel ),'<ul style="list-style:none; display:',$display,'" class="catPai',$idPai,'">',PHP_EOL;
				echo str_repeat( "\t" , $nivel + 1 ),'<li style="width:200px;  id="',$catId,'">
				<input type="checkbox" value="',$catId,'" id="checkbox" class="',$class,'" name="categorias[]">
				<input type="hidden" class="check',$catId,'" value="false">
					<label for="',$catId,'" style="cursor:pointer;" title="',$cadaCat['nome'],'">',$cadaCat['nome'],'
				</label>',PHP_EOL;
				
				echo str_repeat( "\t" , $nivel + 1 ),'<input type="hidden" name="ativa" value="',$catId,'">',PHP_EOL;
			}else{
				$class = "catPai";
				$display = "block";
				echo str_repeat( "\t" , $nivel ),'<ul style="list-style:none; display:',$display,'" class="level-1">',PHP_EOL;
echo str_repeat( "\t" , $nivel + 1 ),'<li style="width:200px;  id="',$catId,'" class="pai">
			<input type="checkbox" value="',$catId,'" id="checkbox" class="',$class,'" name="categorias[]">
				<label for="',$catId,'" style="cursor:pointer;" title="',$cadaCat['nome'],'">',$cadaCat['nome'],'
			</label>',PHP_EOL;
			}
			
			
			
			
			if( isset( $categoriasTotal[$catId] ) )
				$this->montarCategoriasProdutos( $categoriasTotal , $catId , $nivel + 2);
			echo str_repeat( "\t" , $nivel + 1 ),'</li>',PHP_EOL;
			echo str_repeat( "\t" , $nivel ),'</ul>',PHP_EOL;
		}

	}
	public 	function editarCategoriasProdutos( array $categoriasTotal , $idPai = 0, $nivel = 0 ){
		
		foreach( $categoriasTotal[$idPai] as $catId => $cadaCat)
		{	
			
				echo str_repeat( "\t" , $nivel ),'<ul style="list-style:none; class="catPai',$idPai,'">',PHP_EOL;
				echo str_repeat( "\t" , $nivel + 1 ),'<li style="width:200px;  id="',$catId,'">
				<input type="checkbox" value="',$catId,'" checked name="categorias[]">
					<label for="',$catId,'" style="cursor:pointer;" title="',$cadaCat['nome'],'">',$cadaCat['nome'],'
				</label>',PHP_EOL;
			
			if( isset( $categoriasTotal[$catId] ) )
				$this->editarCategoriasProdutos( $categoriasTotal , $catId , $nivel + 2);
				echo str_repeat( "\t" , $nivel + 1 ),'</li>',PHP_EOL;
				echo str_repeat( "\t" , $nivel ),'</ul>',PHP_EOL;
		}
	}

}