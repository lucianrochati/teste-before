<?php

require_once (dirname(dirname(__FILE__))."/Model/dao/Admin.php");

Class Validacoes{
	
	private $AdminDAO;
	
	function __construct(){
		$this->AdminDAO = new AdminDAO();
	}
	public function verificarUsuarioBando($email,$senha){
		
		if($email && $this->validarEmail($email)){
			
			$senha = md5($senha);
			$retorno = $this->AdminDAO->validarUsuarioBanco($email, $senha);
		
			if($retorno == "Autenticado")
				$this->redirectUserAdmin("admin/produtos.php");
			else
				$this->retornoJS("Usuário ou senha inválido!");
		}else{
			$retorno = $this->retornoJS("E-mail inválido!!");
		}
	}
	
	public function validarEmail($email){
		
	    $er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	    if (preg_match($er, $email)){
	  		return true;
	   	 	} else {
	    		return false;
	   	 }
	}

	public function formatarSenha($senha){
		$senha = md5($senha);
		return $senha;
	}
	
	public function retornoJS($msg,$url=false){
		if(!$url)
			$url = "index.php";
			
		die("<script>alert('{$msg}');window.location='{$url}';</script>");
	}
	
	public function redirectUserAdmin($pagina=false){
		if($pagina)
			die("<script>window.location='{$pagina}';	</script>");
		else
			die("<script>window.location='admin/produtos.php';	</script>");
	}
}