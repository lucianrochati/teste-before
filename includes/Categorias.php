<?php

require_once (dirname(dirname(__FILE__))."/includes/Model/dao/Categorias.php");

class Categorias{
	
	private $categoria;
	private $nome;
	
	function  __construct(){
		$this->categoria = new CategoriasDAO();	
	}
	public function getCategorias(){
		return $this->categoria->listarCategorias();

	}
	
	public function getCategoriasPorId($cat){
		return $this->categoria->mostrarCategoriaPeloId($cat);
	}
	
	public function excluirCategoria($id){
		return $this->categoria->excluirCategoria($id);
	}
	
	public function alterarCategoria($array){
		return $this->categoria->alterarCategoria($array);
	}
	
	public function cadastrarCategoria($array){
		return $this->categoria->inserirCategoria($array);
	}
	public function listarCategoriasProduto($categorias){
		return $this->categoria->montarCategoriasProdutos($categorias);
	}
	
	public function editarCategoriasProduto($categorias){
		return $this->categoria->editarCategoriasProdutos($categorias);
	}
	public function setNome($nome){
		$this->nome = $nome;
	}
	public function getNome(){
		return $this->nome;
	}
	
}