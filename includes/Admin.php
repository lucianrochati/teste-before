<?php

require_once("includes/Configuracoes.php");
require_once("Control/Validacoes.php");

/**
 * Enter description here ...
 * @author lucian
 *
 */

class Admin{

	private $nome;
	private $email;
	private $senha;
	private $validacoes;
	
	function __construct(){
		$this->validacoes = new Validacoes();	
	}
	public function validarUsuarioAdmin(){
		
		$this->validacoes->verificarUsuarioBando($this->getEmail(), $this->getSenha());
	
	}
	
	public function setNome($nome){
		$this->nome = $nome;
	}

	public function setEmail($email){
		$this->email = $email;
	}
	
	public function setSenha($senha){
		$this->senha = $senha;
	}
	
	public function getNome(){
		return $this->nome;
	}

	public function getEmail(){
		return $this->email;
	}
	public function getSenha(){
		return $this->senha;
	}
}